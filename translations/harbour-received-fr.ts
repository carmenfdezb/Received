<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>AboutPageForm.ui</name>
    <message>
        <source>About Received</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Report an Issue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Source code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Version: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Translations</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ApiLanguageListModel</name>
    <message>
        <source>English</source>
        <translation>Anglais</translation>
    </message>
    <message>
        <source>German</source>
        <translation>Allemand</translation>
    </message>
    <message>
        <source>Austrian</source>
        <translation>Autrichien</translation>
    </message>
    <message>
        <source>French</source>
        <translation>Français</translation>
    </message>
    <message>
        <source>Portuguese</source>
        <translation>Portugais</translation>
    </message>
    <message>
        <source>Spanish</source>
        <translation>Espagnol</translation>
    </message>
    <message>
        <source>Swedish</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Danish</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Italian</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Polish</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BrowseByCategoryPageForm.ui</name>
    <message>
        <source>Browse by</source>
        <translation>Explorer par</translation>
    </message>
</context>
<context>
    <name>BrowsePage</name>
    <message>
        <source>Local</source>
        <translation>Local</translation>
    </message>
    <message>
        <source>Top 100</source>
        <translation>Top 100</translation>
    </message>
    <message>
        <source>Recommended</source>
        <translation>Recommandé</translation>
    </message>
    <message>
        <source>Genre</source>
        <translation>Genre</translation>
    </message>
    <message>
        <source>Topic</source>
        <translation>Thème</translation>
    </message>
    <message>
        <source>Country</source>
        <translation>Pays</translation>
    </message>
    <message>
        <source>City</source>
        <translation>Ville</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Langue</translation>
    </message>
</context>
<context>
    <name>BrowsePageForm.ui</name>
    <message>
        <source>Browse</source>
        <translation>Explorer</translation>
    </message>
</context>
<context>
    <name>DockedAudioPlayerForm.ui</name>
    <message>
        <source>Sleep timer</source>
        <translation>Minuteur</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <source>Set</source>
        <translation>Définir</translation>
    </message>
</context>
<context>
    <name>FavoritePageForm.ui</name>
    <message>
        <source>http://mystation.com/stream.mp3</source>
        <translation>http://mastation.com/stream.mp3</translation>
    </message>
    <message>
        <source>URL</source>
        <translation>URL</translation>
    </message>
    <message>
        <source>My Station</source>
        <translation>Ma station</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <source>http://mystation.com/logo.jpg</source>
        <translation>http://mastation.com/logo.jpg</translation>
    </message>
    <message>
        <source>Logo</source>
        <translation>Logo</translation>
    </message>
    <message>
        <source>Sweden</source>
        <translation>France</translation>
    </message>
    <message>
        <source>Country</source>
        <translation>Pays</translation>
    </message>
    <message>
        <source>Pop</source>
        <translation>Rock</translation>
    </message>
    <message>
        <source>Genre</source>
        <translation>Genre</translation>
    </message>
</context>
<context>
    <name>FavoritesListContextMenu</name>
    <message>
        <source>Remove from favorite</source>
        <translation>Retirer des favoris</translation>
    </message>
    <message>
        <source>Deleting</source>
        <translation>Suppression</translation>
    </message>
    <message>
        <source>Edit favorite</source>
        <translation>Éditer le favori</translation>
    </message>
</context>
<context>
    <name>FavoritesPageForm.ui</name>
    <message>
        <source>Favorites</source>
        <translation>Favoris</translation>
    </message>
    <message>
        <source>Add Custom</source>
        <translation>Ajouter manuellement une radio</translation>
    </message>
    <message>
        <source>From</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NavigationMenuForm.ui</name>
    <message>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <source>Show player</source>
        <translation>Afficher le lecteur</translation>
    </message>
    <message>
        <source>Browse</source>
        <translation>Explorer</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Rechercher</translation>
    </message>
</context>
<context>
    <name>PlayerLayoutListModel</name>
    <message>
        <source>Original player</source>
        <translation>Taille d&apos;origine</translation>
    </message>
    <message>
        <source>Small player</source>
        <translation>Taille réduite</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Droping DB</source>
        <translation>Réinitialisation</translation>
    </message>
</context>
<context>
    <name>SettingsPageForm.ui</name>
    <message>
        <source>About</source>
        <translation>À propos</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <source>Advanced Options</source>
        <translation>Avancé</translation>
    </message>
    <message>
        <source>Reset DB</source>
        <translation>Réinitialiser la base de données</translation>
    </message>
    <message>
        <source>API Language:</source>
        <translation>Langue API</translation>
    </message>
    <message>
        <source>Player layout</source>
        <translation>Taille du lecteur</translation>
    </message>
    <message>
        <source>API Options</source>
        <translation>API</translation>
    </message>
    <message>
        <source>Basic Options</source>
        <translation>Général</translation>
    </message>
    <message>
        <source>Sets what style to use for player controls</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Removes everything in the database and gives you a clean start. &lt;i&gt;&lt;b&gt;Used with caution&lt;/b&gt;&lt;/i&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sets the endpoint to be used for API calls e.g. radio.net for English and radio.de for German. Also effects Top 100 and translations back from the api</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StationsListContextMenu</name>
    <message>
        <source>Remove from favorite</source>
        <translation>Retirer des favoris</translation>
    </message>
    <message>
        <source>Deleting</source>
        <translation>Suppression</translation>
    </message>
    <message>
        <source>Add to favorite</source>
        <translation>Ajouter aux favoris</translation>
    </message>
</context>
<context>
    <name>StationsPageForm.ui</name>
    <message>
        <source>From</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TranslationCreditsListModel</name>
    <message>
        <source>French</source>
        <translation type="unfinished">Français</translation>
    </message>
    <message>
        <source>Russian</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Spanish</source>
        <translation type="unfinished">Espagnol</translation>
    </message>
    <message>
        <source>Swedish</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TranslationCreditsPageForm.ui</name>
    <message>
        <source>Translations</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Utils</name>
    <message>
        <source>Search station</source>
        <translation>Rechercher une station</translation>
    </message>
    <message>
        <source>Top 100</source>
        <translation>Top 100</translation>
    </message>
    <message>
        <source>Favorites</source>
        <translation>Favoris</translation>
    </message>
    <message>
        <source>Recommended</source>
        <translation>Recommandé</translation>
    </message>
    <message>
        <source>Local</source>
        <translation>Local</translation>
    </message>
</context>
</TS>
