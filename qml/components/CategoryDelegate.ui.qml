import QtQuick 2.6
import Sailfish.Silica 1.0
import "../components"

ListItem {
    id: listItem
    contentHeight: Theme.itemSizeSmall
    
    Label {
        text: title
        font.bold: true
        color: Theme.primaryColor;
        anchors.fill: parent
        verticalAlignment: Text.AlignVCenter
        
        anchors {
            left: parent.left
            leftMargin: Theme.horizontalPageMargin
            right: parent.right
            rightMargin: Theme.horizontalPageMargin
        }
    }
}
