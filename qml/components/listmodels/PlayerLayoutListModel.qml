import QtQuick 2.0

ListModel {
     id: model
     ListElement { title: qsTr("Original player"); fileName: "DockedAudioPlayer" }
     ListElement { title: qsTr("Small player"); fileName: "DockedAudioPlayerSmall" }
 }
