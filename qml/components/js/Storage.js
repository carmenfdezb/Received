.pragma library
.import QtQuick.LocalStorage 2.0 as Storage

var db = null

var settings = {
    name: "Received",
    version: "",
    description: "unofficial rad.io client",
    size: 1000000,
    latestVersion: 11
}

var dbVersions = {
    1: 'CREATE TABLE IF NOT EXISTS stations(id INTEGER PRIMARY KEY,\
            name TEXT, pictureBaseURL TEXT, picture1Name TEXT, country TEXT, genre TEXT);',
    2: 'CREATE TABLE IF NOT EXISTS SleepTimer(hour INTEGER, minute INTEGER)',
    3: 'INSERT INTO SleepTimer values(0, 20)',
    4: 'ALTER TABLE stations ADD COLUMN stationLogo TEXT',
    5: 'UPDATE stations SET stationLogo=pictureBaseURL||picture1Name',
    6: 'CREATE TEMPORARY TABLE station_backup(name, stationLogo, country, genre, radIoId)',
    7: 'INSERT INTO station_backup SELECT name, stationLogo, country, genre, id FROM stations',
    8: 'DROP TABLE stations',
    9: 'CREATE TABLE stations(url TEXT UNIQUE, name TEXT, stationLogo TEXT, \
            country TEXT, genre TEXT, radIoId INTEGER)',
    10: 'INSERT INTO stations(url, name, stationLogo, country, genre, radIoId) \
            SELECT rowid, name, stationLogo, country, genre, radIoId FROM station_backup',
    11: 'DROP TABLE station_backup'

}

function openDB() {
    if(db !== null)
        return;

    db = Storage.LocalStorage.openDatabaseSync(settings.name,
                                               settings.version,
                                               settings.description,
                                               settings.size);

    var currentVersion = (db.version === "") ? 0 : parseInt(db.version);
    console.log("CURRENT DB VERSION: " + currentVersion)

    // Update database to latest version
    for(; currentVersion < settings.latestVersion; ++currentVersion) {
        var nextVersion = currentVersion+1;
        db.changeVersion(db.version, nextVersion, function(tx) {
            tx.executeSql(dbVersions[nextVersion]);
            console.log('Database updated to: '+nextVersion);
        });

        db = Storage.LocalStorage.openDatabaseSync(settings.name,
                                                   settings.version,
                                                   settings.description,
                                                   settings.size);
    }
}

function loadStations() {
    var results;
    openDB();
    db.transaction(function(tx) {
        results = tx.executeSql("SELECT * FROM stations");
    });
    return results;
}


/**
 * Save a station in database
 *
 * If the station has the property "id" it's a staition from rad.io and we
 * save that id under radIoId if not we insert null for that field
 */
function saveStation(station) {
    var result;
    openDB();
    db.transaction(function(tx) {
        result = tx.executeSql('REPLACE INTO stations (url, name, stationLogo, country, genre, radIoId) \
                                    VALUES(?, ?, ?, ?, ?, ?)', [station.url, station.name, station.stationLogo,
                                                                station.country, station.genre, station.radIoId || null])

    });
    return result;
}

function deleteStation(station) {
    var result;
    openDB();
    db.transaction(function(tx) {
        result = tx.executeSql('DELETE FROM stations WHERE url = ?', [station.url])
    });
    return result;
}

function stationExists(station) {
    var results;
    console.log("stationExists?", station)
    if (!station) { return false }

    openDB();
    db.transaction(function(tx) {
        results = tx.executeSql("SELECT * FROM stations WHERE url=?", [station.url]);
    });
    console.log("STATION EXISTS RESULTS: " + results.rows.length)
    return results.rows.length === 1
}

function stationExistsByRadIoId(id) {
    var results;
    console.log("stationExistsByRadIoId", id)
    openDB();
    db.transaction(function(tx) {
        results = tx.executeSql("SELECT * FROM stations WHERE radIoId=?", [id]);
    });
    console.log("STATION EXISTS RESULTS: " + results.rows.length)
    return results.rows.length === 1
}

function findByRadIoId(id) {
    var results;
    console.log("findByRadIoId", id)
    openDB();
    db.transaction(function(tx) {
        results = tx.executeSql("SELECT * FROM stations WHERE radIoId=?", [id]);
    });
    console.log("STATION EXISTS RESULTS: " + results.rows.length)
    return results.rows.item(0);
}

function loadSleepTimer() {
    var results;
    openDB();
    db.transaction(function(tx) {
        results = tx.executeSql("SELECT hour, minute FROM SleepTimer LIMIT 1");
    });
    return results.rows.item(0);
}

function updateSleepTimer(h, m) {
    var result;
    openDB();
    db.transaction(function(tx) {
        result = tx.executeSql('UPDATE SleepTimer SET hour=?, minute=?', [h, m])

    });
    return result;
}

function dropDB() {
    db.changeVersion(db.version, "", function(tx) {
        tx.executeSql("DROP TABLE IF EXISTS stations");
        console.log("db dropt")
    });

    db = null;
}
