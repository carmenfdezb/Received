import QtQuick 2.6
import Sailfish.Silica 1.0
import "../js/Favorites.js" as FavoritesUtils

/**
 * Context menu intended for use within StationsPage list
 *
 * index is coming from that list and is the index in the list that the contextmenu was triggered for
 */
ContextMenu {
    signal removeFavorite(var station)
    signal addFavorite(int id)

    MenuItem {
        id: removeFavoritMenuItem
        text: qsTr("Remove from favorite")
        onClicked: {
            remorseAction(qsTr("Deleting"), function() {
                removeFavorite(FavoritesUtils.getFavoriteByRadioId(id))
            });
        }
    }

    MenuItem {
        id: addFavoritMenuItem
        text: qsTr("Add to favorite")
        onClicked: {
            addFavorite(id)
        }
    }

    onActiveChanged: {
        var isFavorit = FavoritesUtils.isRadIoFavorite(id)
        removeFavoritMenuItem.visible = isFavorit
        addFavoritMenuItem.visible = !isFavorit
    }
}
