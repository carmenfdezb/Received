import QtQuick 2.6
import Sailfish.Silica 1.0
import "../components"


SearchField {
    id: searchField

    signal search(string text)
    
    EnterKey.enabled: text.length > 0
    EnterKey.iconSource: "image://theme/icon-m-enter-accept"
    EnterKey.onClicked: {
        if(text != "") {
            search(text);
        }
        else {
            searchField.focus = false;
        }
    }
}
