import QtQuick 2.6
import Sailfish.Silica 1.0

AboutPageForm {
    issueAction.onClicked: Qt.openUrlExternally("https://gitlab.com/sailfish-apps/Received/issues/new")
    sourceAction.onClicked: Qt.openUrlExternally("https://gitlab.com/sailfish-apps/Received/")
    translationAction.onClicked: pageStack.push(Qt.resolvedUrl("TranslationCreditsPage.qml"));
}
