import QtQuick 2.6
import "../components/js/Utils.js" as Utils

BrowseByCategoryPageForm {

    headerTitle: headerTitle.toLocaleLowerCase()

    browseByListView.onCurrentIndexChanged: {
        var item = browseByModel.get(browseByListView.currentIndex)
        pageStack.push(Qt.resolvedUrl("StationsPage.qml"), {listType: new Utils.ListType(item.title),
                           category: item.category, value: item.name});
    }


    // Other

    // Set a custom search action in navigation menu
    navigationMenu.searchAction: function() {
        pageStack.replaceAbove(
                    pageStack.find(function(page) {
                        return page.firstPage === true}),
                    Qt.resolvedUrl("StationsPage.qml"),
                    {listType: Utils.Search});
    }

    Component.onCompleted: {
        switch(category.toLocaleLowerCase()) {
        case "genre":
            radioAPI.getGenres(browseByModel);
            break;
        case "topic":
            radioAPI.getTopics(browseByModel);
            break;
        case "country":
            radioAPI.getCountries(browseByModel);
            break;
        case "city":
            radioAPI.getCities(browseByModel);
            break;
        case "language":
            radioAPI.getLanguages(browseByModel);
            break;
        default:
            console.error("Browsing by unhandled category", category)
            break;
        }
    }
}
