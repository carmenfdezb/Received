import QtQuick 2.6
import Sailfish.Silica 1.0
import "../components"

Page {
    id: browsePage

    signal categorySelected(int index)

    property alias browseListView: browseListView
    property alias browseModel: browseModel

    SilicaListView {
        id: browseListView
        VerticalScrollDecorator { flickable: browseListView }
        anchors.fill: parent

        NavigationMenu {
            id: pulleyMeny
            hideBrowseAction: true
        }

        header: Column {
                    PageHeader {
                        title: qsTr("Browse")
                        width: page.width
                    }
                }

        currentIndex: -1

        model: ListModel {
            id: browseModel
        }

        delegate: CategoryDelegate {
            id: categoryItem

            Connections {
                target: categoryItem
                onClicked: browseListView.currentIndex = index
            }
        }
    }
}
