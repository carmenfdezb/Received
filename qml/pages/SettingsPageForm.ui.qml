import QtQuick 2.6
import Sailfish.Silica 1.0
import "../components/listmodels/"

Page {
    id: settingsPage

    signal apiLanguageChanged(string title, string url)
    signal playerLayoutChanged(string fileName)

    property alias aboutAction: aboutAction
    property alias buttonResetDb: buttonResetDb
    property alias playerLayoutComboBox: playerLayoutComboBox
    property alias playerLayoutModel: playerLayoutModel
    property alias apiLanguageComboBox: apiLanguageComboBox
    property alias apiLanguageModel: apiLanguageModel

    SilicaFlickable {
        id: settingsPageFlickable
        anchors.fill: parent
        contentHeight: settingsColumn.height
        VerticalScrollDecorator { flickable: settingsPageFlickable }

        PullDownMenu {
            MenuItem {
                id: aboutAction
                text: qsTr("About")
            }
        }
        RemorsePopup { id: remorse }

        Column {
            id: settingsColumn
            spacing: Theme.paddingSmall
            anchors {
                left: parent.left
                right: parent.right
            }

            PageHeader {
                title: qsTr("Settings")
            }

            SectionHeader {
                text: qsTr("Basic Options")
            }

            ComboBox {
                id: playerLayoutComboBox
                anchors.leftMargin: Theme.horizontalPageMargin
                anchors.rightMargin: Theme.horizontalPageMargin

                label: qsTr("Player layout")
                description: qsTr("Sets what style to use for player controls")

                menu: ContextMenu {
                    id: playerLayoutComboBoxMenu

                    Repeater {
                    model: PlayerLayoutListModel { id: playerLayoutModel }
                        MenuItem {
                            id: playerLayout
                            text: model.title

                            Connections {
                                target: playerLayout
                                onClicked: playerLayoutChanged(model.fileName)
                            }
                        }
                    }
                }
            }

            SectionHeader {
                text: qsTr("API Options")
            }

            ComboBox {
                id: apiLanguageComboBox
                anchors.leftMargin: Theme.horizontalPageMargin
                anchors.rightMargin: Theme.horizontalPageMargin

                label: qsTr("API Language:")
                description: qsTr("Sets the endpoint to be used for API calls e.g. radio.net for English and radio.de for German. Also effects Top 100 and translations back from the api")

                menu: ContextMenu {
                    id: apiLanguageComboBoxMenu
                    Repeater {
                        model: ApiLanguageListModel { id: apiLanguageModel }
                        MenuItem {
                            id: apiLanguage
                            text: model.title

                            Connections {
                                target: apiLanguage
                                onClicked: apiLanguageChanged(model.title, model.apiUrl)
                            }
                        }
                    }
                }
            }

            SectionHeader {
                text: qsTr("Advanced Options")
            }

            Column {
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.leftMargin: Theme.horizontalPageMargin
                anchors.rightMargin: Theme.horizontalPageMargin

                Button {
                    id: buttonResetDb
                    text: qsTr("Reset DB")
                }

                Text {
                    width: parent.width
                    font.pixelSize: Theme.fontSizeExtraSmall
                    wrapMode: Text.Wrap
                    color: Theme.secondaryColor
                    text: qsTr("Removes everything in the database and gives you a clean start. <i><b>Used with caution</b></i>")
                }
            }
        }
    }
}
